package systemd

import (
	"bytes"
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"text/template"

	"github.com/fatih/color"
)

type ServiceTemplate struct {
	Description, ExecStart, WorkingDir, Environment, After, Require string
	RestartSeconds                                                  int
}

func (c *ServiceTemplate) UnmarshalYAML(unmarshal func(any) error) error {
	var dict map[string]any
	if err := unmarshal(&dict); err != nil {
		var str string
		if err := unmarshal(&str); err != nil {
			return err
		}
		c.ExecStart = str
		return nil
	}

	for k, v := range dict {
		var ok bool
		switch k {
		case "cmd":
			c.ExecStart, ok = v.(string)
		case "desc":
			c.Description, ok = v.(string)
		case "dir":
			c.WorkingDir, ok = v.(string)
		case "env":
			c.Environment, ok = v.(string)
		case "after":
			c.After, ok = v.(string)
		case "require":
			c.Require, ok = v.(string)
		case "restart":
			c.RestartSeconds, ok = v.(int)
		default:
			return fmt.Errorf("invalid key: %s", k)
		}
		if !ok {
			return fmt.Errorf("invalid %s type", k)
		}
	}
	if c.ExecStart == "" {
		return errors.New("missing cmd")
	}

	return nil
}

var serviceTemplate = template.Must(template.New("name").Parse(`[Unit]
Description={{.Description}}{{if .After}}
After={{.After}}{{end}}{{if .Require}}
Require={{.Require}}{{end}}

[Service]
Type=simple
ExecStart={{.ExecStart}}
WorkingDirectory={{.WorkingDir}}{{if .Environment}}
Environment={{.Environment}}{{end}}{{if .RestartSeconds}}
Restart=always
RestartSec={{.RestartSeconds}}{{end}}
`))

func (st ServiceTemplate) BuildTemplate(name, pwd string) string {
	if st.WorkingDir == "" {
		st.WorkingDir = pwd
	}
	if st.Description == "" {
		st.Description = name
	}
	buf := new(bytes.Buffer)
	serviceTemplate.Execute(buf, st)
	return buf.String()
}

func (st ServiceTemplate) WriteFile(name, pwd string) {
	fpath := filepath.Join(SYSTEMD_USER_DIR, name+".service")
	err := os.WriteFile(fpath, []byte(st.BuildTemplate(name, pwd)), 0644)
	if err != nil {
		color.Red("Unable to write service file\n%v", err)
		os.Exit(1)
	}
	color.White("Service file written\n%s", fpath)
}
