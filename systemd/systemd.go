package systemd

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"codeberg.org/lil5/task-services/command"
	"github.com/fatih/color"
)

func Reload() {
	code, err := command.Shell("", "systemctl --user daemon-reload")
	if err != nil {
		color.Red("Unable to reload systemctl\n%v", err)
		os.Exit(code)
	}
}
func Start(name string) {
	code, err := command.Shell("", "systemctl --user start "+name)
	if err != nil {
		color.Red("Unable to start systemctl\n%v", err)
		os.Exit(code)
	}
}
func Restart(name string) {
	code, err := command.Shell("", "systemctl --user restart "+name)
	if err != nil {
		color.Red("Unable to restart systemctl\n%v", err)
		os.Exit(code)
	}
}
func Stop(name string) {
	code, err := command.Shell("", "systemctl --user stop "+name)
	if err != nil {
		color.Red("Unable to stop systemctl\n%v", err)
		os.Exit(code)
	}
}
func Enable(name string) {
	code, err := command.Shell("", "systemctl --user enable "+name)
	if err != nil {
		color.Red("Unable to enable systemctl\n%v", err)
		os.Exit(code)
	}
}
func Disable(name string) {
	code, err := command.Shell("", "systemctl --user disable "+name)
	if err != nil {
		color.Red("Unable to disable systemctl\n%v", err)
		os.Exit(code)
	}
}
func Status(name string) {
	code, err := command.Shell("", "systemctl --user status "+name)
	if err != nil && code != 3 {
		color.Red("Unable to get status from systemctl\n%v", err)
		os.Exit(code)
	}
}
func Logs(name string, lines int) {
	c := fmt.Sprintf("journalctl --user -u %s", name)
	if lines != 0 {
		c = fmt.Sprintf("%s -n %d", c, lines)
	}
	code, err := command.Shell("", c)
	if err != nil {
		color.Red("Unable to get status from systemctl\n%v", err)
		os.Exit(code)
	}
}

func ReadDir() []string {
	entries, err := os.ReadDir(SYSTEMD_USER_DIR)
	if err != nil {
		color.Red("Unable to walk user systemd directory\n%v", err)
		os.Exit(1)
	}
	serviceFiles := []string{}
	for _, entry := range entries {
		if entry.IsDir() {
			continue
		}
		name := entry.Name()
		if !strings.HasSuffix(name, ".service") {
			continue
		}
		name = strings.TrimSuffix(name, ".service")
		serviceFiles = append(serviceFiles, name)
	}
	return serviceFiles
}

func FindByName(name string) bool {
	entries, err := os.ReadDir(SYSTEMD_USER_DIR)
	if err != nil {
		color.Red("Unable to walk user systemd directory\n%v", err)
		os.Exit(1)
	}
	for _, entry := range entries {
		if entry.IsDir() {
			continue
		}
		n := entry.Name()
		if !strings.HasSuffix(n, ".service") {
			continue
		}
		n = strings.TrimSuffix(n, ".service")
		if n == name {
			return true
		}
	}
	return false
}

var SYSTEMD_USER_DIR string

func init() {
	home, err := os.UserHomeDir()
	if err != nil {
		color.Red("Unable to get home directory\n%v", err)
		os.Exit(1)
	}

	SYSTEMD_USER_DIR = filepath.Join(home, ".config/systemd/user/")
}

func CreateIfNotExistsDir() {
	err := os.MkdirAll(SYSTEMD_USER_DIR, os.ModePerm)
	if err != nil {
		color.Red("Unable to create user systemd directory\n%v", err)
		os.Exit(1)
	}
}

func RemoveFile(name string) {
	fpath := filepath.Join(SYSTEMD_USER_DIR, name+".service")
	err := os.Remove(fpath)
	if err != nil {
		color.Red("Unable to remove systemd service\n%v", err)
		os.Exit(1)
	}
}
