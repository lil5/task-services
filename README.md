# Task Services

## Example

Taskfile.yml - Adds services

```yml
version: "3"

tasks:
   app_build: go build -o app . 
   app_start: ./app

services:
   my_app: ./app --port=8000 --mode=production
```

Run commands

```
$ task app_build
go build -o app .

$ task-services create my_app
Service file written
/home/user/.config/systemd/user/my_app.service
systemctl --user daemon-reload

$ task-services start my_app
systemctl --user start my_app
$ task-services enable my_app
systemctl --user enable my_app

$ task-services destroy my_app
systemctl --user stop my_app
systemctl --user disable my_app
rm /home/user/.config/systemd/user/my_app.service
systemctl --user daemon-reload
```


```
$ task-services -h
NAME:
   task-services - Manage your application processes in production hassle-free like Heroku CLI with go-task and Systemd

USAGE:
   task-services [global options] command [command options] 

DESCRIPTION:
   I do like the way how simple is managing of application processes in production on Heroku with Procfile. How easily can be accessed application logs with heroku logs command. Just type heroku create and you're good to go.

   Can we have something similar on the cheap Ubuntu VPS from DigitalOcean? Yes we can, all we need is a systemd wrapper which allows to export application processes from Taskfile.yml to system services, and control them/check status/access logs using simple commands.

COMMANDS:
   create, c              Create and enable app services
   destroy, d             Stop, disable and remove app services
   start, s               Start app services
   stop, p                Stop app services
   restart, r             Restart app services
   enable, en             Enable app target
   disable, dis           Disable app target
   logs, log              Show app services logs
   status, stat, info, i  Show app services status
   list, ls               List all app services
   exec, run, execute     Run single app process outside of systemd
   help, h                Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --help, -h  show help
```
 
# License

MIT

Inspired by https://github.com/vifreefly/procsd [@vifreefly](https://github.com/vifreefly)