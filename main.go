package main

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"runtime"
	"strings"

	"codeberg.org/lil5/task-services/command"
	"codeberg.org/lil5/task-services/systemd"
	"codeberg.org/lil5/task-services/taskfile"
	"github.com/fatih/color"
	"github.com/urfave/cli/v2"
	"gopkg.in/yaml.v3"
)

func main() {
	app := &cli.App{
		Name:  "task-services",
		Usage: "Manage your application processes in production hassle-free like Heroku CLI with go-task and Systemd",
		Description: `I do like the way how simple is managing of application processes in production on Heroku with Procfile. How easily can be accessed application logs with heroku logs command. Just type heroku create and you're good to go.

Can we have something similar on the cheap Ubuntu VPS from DigitalOcean? Yes we can, all we need is a systemd wrapper which allows to export application processes from Taskfile.yml to system services, and control them/check status/access logs using simple commands.`,
		Commands: []*cli.Command{
			// "create"
			{
				Name:    "create",
				Aliases: []string{"c"},
				Usage:   "Create and enable app services",
				Flags: []cli.Flag{
					&cli.BoolFlag{
						Name:  "or-restart",
						Value: false,
						Usage: "If option provided and service already created, will skip creation and call instead restart command. Otherwise (if service is not present), they will be created and (in addition) started.",
					},
				},
				Action: func(ctx *cli.Context) error {
					isLinuxOrErr(ctx)
					name := getName(ctx)

					systemd.CreateIfNotExistsDir()
					pwd := getPwd()
					tf := readTaskfile(pwd)
					exists := tf.GetExec(name) != nil
					fOrRestart := ctx.Bool("or-restart")
					if fOrRestart {
						if exists {
							systemd.Restart(name)
						} else {
							tf.CreateService(pwd, name)
							systemd.Start(name)
						}
					} else {
						tf.CreateService(pwd, name)
					}

					return nil
				},
			},
			// "destroy"
			{
				Name:    "destroy",
				Aliases: []string{"d"},
				Usage:   "Stop, disable and remove app services",
				Action: func(ctx *cli.Context) error {
					isLinuxOrErr(ctx)
					name := getName(ctx)

					systemd.CreateIfNotExistsDir()
					pwd := getPwd()
					tf := readTaskfile(pwd)
					tf.MustFindByName(name)
					exists := systemd.FindByName(name)
					if !exists {
						color.Yellow("Service %s is not found", name)
						os.Exit(0)
					}
					systemd.Stop(name)
					systemd.Disable(name)
					systemd.RemoveFile(name)
					systemd.Reload()

					return nil
				},
			},
			// "start"
			{
				Name:    "start",
				Aliases: []string{"s"},
				Usage:   "Start app services",
				Action: func(ctx *cli.Context) error {
					isLinuxOrErr(ctx)
					name := getName(ctx)

					systemd.CreateIfNotExistsDir()
					pwd := getPwd()
					tf := readTaskfile(pwd)
					tf.MustFindByName(name)
					exists := systemd.FindByName(name)
					if !exists {
						color.Yellow("Service %s is not found", name)
						os.Exit(0)
					}
					systemd.Start(name)

					return nil
				},
			},
			// "stop"
			{
				Name:    "stop",
				Aliases: []string{"p"},
				Usage:   "Stop app services",
				Action: func(ctx *cli.Context) error {
					isLinuxOrErr(ctx)
					name := getName(ctx)

					systemd.CreateIfNotExistsDir()
					pwd := getPwd()
					tf := readTaskfile(pwd)
					tf.MustFindByName(name)
					exists := systemd.FindByName(name)
					if !exists {
						color.Yellow("Service %s is not found", name)
						os.Exit(0)
					}
					systemd.Stop(name)

					return nil
				},
			},
			// "restart"
			{
				Name:    "restart",
				Aliases: []string{"r"},
				Usage:   "Restart app services",
				Action: func(ctx *cli.Context) error {
					isLinuxOrErr(ctx)
					name := getName(ctx)

					systemd.CreateIfNotExistsDir()
					pwd := getPwd()
					tf := readTaskfile(pwd)
					tf.MustFindByName(name)
					exists := systemd.FindByName(name)
					if !exists {
						color.Yellow("Service %s is not found", name)
						os.Exit(0)
					}
					systemd.Restart(name)

					return nil
				},
			},
			// "enable"
			{
				Name:    "enable",
				Aliases: []string{"en"},
				Usage:   "Enable app target",
				Action: func(ctx *cli.Context) error {
					isLinuxOrErr(ctx)
					name := getName(ctx)

					systemd.CreateIfNotExistsDir()
					pwd := getPwd()
					tf := readTaskfile(pwd)
					tf.MustFindByName(name)
					exists := systemd.FindByName(name)
					if !exists {
						color.Yellow("Service %s.service is not found", name)
						os.Exit(0)
					}
					systemd.Enable(name)

					return nil
				},
			},
			// "disable"
			{
				Name:    "disable",
				Aliases: []string{"dis"},
				Usage:   "Disable app target",
				Action: func(ctx *cli.Context) error {
					isLinuxOrErr(ctx)
					name := getName(ctx)

					systemd.CreateIfNotExistsDir()
					pwd := getPwd()
					tf := readTaskfile(pwd)
					tf.MustFindByName(name)
					exists := systemd.FindByName(name)
					if !exists {
						color.Yellow("Service %s is not found", name)
						os.Exit(0)
					}
					systemd.Disable(name)

					return nil
				},
			},
			// "logs"
			{
				Name:    "logs",
				Aliases: []string{"log"},
				Usage:   "Show app services logs",
				Flags: []cli.Flag{
					&cli.IntFlag{
						Name:    "lines",
						Aliases: []string{"n"},
						Value:   0,
						Usage:   "Number of journal entries to show.",
					},
				},
				Action: func(ctx *cli.Context) error {
					isLinuxOrErr(ctx)
					name := getName(ctx)

					systemd.CreateIfNotExistsDir()
					pwd := getPwd()
					tf := readTaskfile(pwd)
					tf.MustFindByName(name)
					exists := systemd.FindByName(name)
					if !exists {
						color.Yellow("Service %s is not found", name)
						os.Exit(0)
					}
					lines := ctx.Int("lines")
					systemd.Logs(name, lines)

					return nil
				},
			},
			// "status"
			{
				Name:    "status",
				Aliases: []string{"stat", "info", "i"},
				Usage:   "Show app services status",
				Action: func(ctx *cli.Context) error {
					isLinuxOrErr(ctx)
					name := getName(ctx)

					systemd.CreateIfNotExistsDir()
					pwd := getPwd()
					tf := readTaskfile(pwd)
					tf.MustFindByName(name)
					exists := systemd.FindByName(name)
					if !exists {
						color.Yellow("Service %s is not found", name)
						os.Exit(0)
					}
					systemd.Status(name)

					return nil
				},
			},
			// "list"
			{
				Name:    "list",
				Aliases: []string{"ls"},
				Usage:   "List all app services",
				Action: func(ctx *cli.Context) error {
					isLinuxOrErr(ctx)

					systemd.CreateIfNotExistsDir()
					pwd := getPwd()
					tf := readTaskfile(pwd)
					sdDir := systemd.ReadDir()

					for k := range tf.Services {
						loaded := ""
						for _, s := range sdDir {
							if s == k {
								loaded = "loaded"
								break
							}
						}

						fmt.Printf("%s% 52s\n", k, loaded)
					}

					return nil
				},
			},
			// "exec"
			{
				Name:    "exec",
				Aliases: []string{"run", "execute"},
				Usage:   "Run single app process outside of systemd",
				Action: func(ctx *cli.Context) error {
					isUnuxOrErr(ctx)
					name := getName(ctx)

					pwd := getPwd()
					tf := readTaskfile(pwd)
					tf.MustFindByName(name)
					exec := tf.GetExec(name)

					start := exec.ExecStart
					if exec.WorkingDir != "" {
						start = fmt.Sprintf("cd %s; %s", exec.WorkingDir, start)
					}

					code, err := command.Shell(pwd, start)
					if err != nil {
						color.Red("err: %v", err)
						os.Exit(code)
					}

					return nil
				},
			},
		},
	}
	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}

func isLinuxOrErr(ctx *cli.Context) {
	if runtime.GOOS != "linux" {
		color.White("%s %s is only supported on Linux", ctx.App.Name, ctx.Command.Name)
		os.Exit(1)
	}
}
func isUnuxOrErr(ctx *cli.Context) {
	switch runtime.GOOS {
	case "linux", "darwin":
		return
	default:
		color.White("%s %s is only supported on Linux & MacOS", ctx.App.Name, ctx.Command.Name)
		os.Exit(1)
	}
}
func getName(ctx *cli.Context) string {
	name := ctx.Args().First()
	name = strings.TrimSuffix(name, ".service")
	if name == "" {
		color.Yellow("Service name not specified")
		os.Exit(1)
	}
	return name
}

func getPwd() string {
	pwd, err := os.Getwd()
	if err != nil {
		color.Red("Unable to get current directory\n%v", err)
		os.Exit(1)
	}
	return pwd
}

func readTaskfile(pwd string) *taskfile.Taskfile {
	t, err := func() (*taskfile.Taskfile, error) {
		b, err := os.ReadFile(filepath.Join(pwd, "Taskfile.yml"))
		if err != nil {
			return nil, err
		}

		var taskfile *taskfile.Taskfile
		err = yaml.Unmarshal(b, &taskfile)
		if err != nil {
			return nil, err
		}
		return taskfile, nil
	}()
	if err != nil {
		color.Red("Unable to read Taskfile\n%v", err)
		os.Exit(1)
	}
	return t
}
