package command

import (
	"fmt"
	"os"
	"os/exec"
)

func Shell(pwd, command string) (int, error) {
	c := exec.Command("/bin/sh", "-c", command)
	if pwd != "" {
		command = fmt.Sprintf("cd %s; %s", pwd, command)
	}
	c.Env = os.Environ()
	c.Stdout = os.Stdout
	c.Stderr = os.Stderr

	fmt.Println(command)
	err := c.Run()
	if err == nil {
		return 0, nil
	}
	exitError, ok := (err).(*exec.ExitError)
	if !ok {
		return 1, err
	}

	return exitError.ExitCode(), err
}
