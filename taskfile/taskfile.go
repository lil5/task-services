package taskfile

import (
	"os"

	"codeberg.org/lil5/task-services/systemd"
	"github.com/fatih/color"
)

type Taskfile struct {
	Services map[string]*systemd.ServiceTemplate `yaml:"services"`
}

func (tf *Taskfile) MustFindByName(name string) *systemd.ServiceTemplate {
	execStart := tf.GetExec(name)
	if execStart == nil {
		color.Yellow("Unable find service in taskfile\nkey: %s, value: %s", name, execStart)
		os.Exit(1)
	}
	return execStart
}
func (tf *Taskfile) GetExec(name string) *systemd.ServiceTemplate {
	for k, v := range tf.Services {
		if k == name {
			return v
		}
	}
	return nil
}

func (tf *Taskfile) CreateService(pwd, name string) {
	st := tf.Services[name]
	st.WriteFile(name, pwd)
	systemd.Reload()
}

func (tf *Taskfile) RestartService(name string) {
	tf.MustFindByName(name)
	systemd.Restart(name)
}
