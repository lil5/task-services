package taskfile

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gopkg.in/yaml.v3"
)

func TestTaskfileUnmarchal(t *testing.T) {
	f := `
services:
  one: "echo 'one'"
  two: 
    desc: Echos current dir
    cmd: pwd
    dir: /
    env: "HI=lorem HRISEH=ipsum"
    restart: 3
    require: mariadb.service
    after: mariadb.service
  three:
    desc: Echos current dir
    cmd: pwd
`
	var ts Taskfile
	err := yaml.Unmarshal([]byte(f), &ts)
	if err != nil {
		assert.FailNow(t, "%w", err)
		return
	}

	t.Run("one", func(t *testing.T) {
		assert.Equal(t, ts.Services["one"].ExecStart, "echo 'one'")

		oneServiceFile := ts.Services["one"].BuildTemplate("one", "/home/user")
		oneServiceFileExpected := `[Unit]
Description=one

[Service]
Type=simple
ExecStart=echo 'one'
WorkingDirectory=/home/user
`
		assert.Equal(t, oneServiceFileExpected, oneServiceFile)
	})

	t.Run("two", func(t *testing.T) {
		assert.Equal(t, ts.Services["two"].ExecStart, "pwd")
		assert.Equal(t, ts.Services["two"].Description, "Echos current dir")
		assert.Equal(t, ts.Services["two"].WorkingDir, "/")
		assert.Equal(t, ts.Services["two"].Environment, "HI=lorem HRISEH=ipsum")
		assert.Equal(t, ts.Services["two"].RestartSeconds, 3)
		assert.Equal(t, ts.Services["two"].After, "mariadb.service")
		assert.Equal(t, ts.Services["two"].Require, "mariadb.service")

		twoServiceFile := ts.Services["two"].BuildTemplate("two", "/opt")
		twoServiceFileExpected := `[Unit]
Description=Echos current dir
After=mariadb.service
Require=mariadb.service

[Service]
Type=simple
ExecStart=pwd
WorkingDirectory=/
Environment=HI=lorem HRISEH=ipsum
Restart=always
RestartSec=3
`
		assert.Equal(t, twoServiceFileExpected, twoServiceFile)
	})
	t.Run("three", func(t *testing.T) {
		assert.Equal(t, ts.Services["three"].ExecStart, "pwd")
		assert.Equal(t, ts.Services["three"].Description, "Echos current dir")
		assert.Equal(t, ts.Services["three"].WorkingDir, "")
		assert.Equal(t, ts.Services["three"].Environment, "")
		assert.Equal(t, ts.Services["three"].RestartSeconds, 0)
		assert.Equal(t, ts.Services["three"].After, "")

		threeServiceFile := ts.Services["three"].BuildTemplate("three", "/usr/bin")
		threeServiceFileExpected := `[Unit]
Description=Echos current dir

[Service]
Type=simple
ExecStart=pwd
WorkingDirectory=/usr/bin
`
		assert.Equal(t, threeServiceFileExpected, threeServiceFile)
	})
}
